from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, Http404
from .models import Question

# Create your views here.

def index(request):
	# response = HttpResponse('Hello, here is the index.')
	# return response
	latest_question_list = Question.objects.order_by('-pub_date')[:5]
	# output = ', '.join([q.question_text for q in latest_question_list])
	# return HttpResponse(output)
	context = {'latest_question_list': latest_question_list}
	return render(request, 'polls/index.html', context)

def detail(request, question_id):
	# return HttpResponse('這是 %s 的詳細頁面' % question_id)
	# try:
	# 	question = Question.objects.get(pk = question_id)
	# except Question.DoesNotExist:
	# 	raise Http404("這個問題並不存在！")

	question = get_object_or_404(Question, pk=question_id)

	question = Question.objects.filter(pk=question_id)
	if len(question) == 0:
		return render(request, 'polls/detail.html', {'question': '沒有東西'})
	else:
		return render(request, 'polls/detail.html', {'question': question[0]})

	return render(request, 'polls/detail.html', {'question':question})


def results(request, question_id):
	response = '這是第 %s 個問題的投票結果。'
	return HttpResponse(response % question_id)

def vote(request, question_id):
	response = '投票給第 %d 個問題' % question_id
	return HttpResponse(response)