from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('<int:question_id>/', views.detail, name='詳細資料'),
    path('<int:question_id>/results/', views.results, name='投票結果'),
    path('<int:question_id>/vote/', views.vote, name='投票')
]